require 'test_helper'

class SignedOutUserTest < ActionDispatch::IntegrationTest

  test "cannot access certain pages when not logged in" do
    get_via_redirect '/posts/new'
    assert_equal '/users/sign_in', path

    post_via_redirect "/posts", "post"=>{"content"=>"hello"}, "commit"=>"Create Post"
    assert_equal '/users/sign_in', path

    post = Post.first
    get_via_redirect edit_post_path(post)
    assert_equal '/users/sign_in', path
  end

  test "display content but not edit link" do
    post = Post.first
    get post_path(post)
    assert_select 'p', post.content
    assert_select 'a', {count: 0, text: 'Edit'}
  end

  test "visit signin page" do
    get "/users/sign_in"
    assert_response :success
  end

end