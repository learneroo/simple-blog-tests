This app is set up with a User model and Devise, and includes tests. Your goal is to create a simple blog platform that passes the tests!

Simple Blog
================

A simple blog platform that only allows writing with the most common 1100 words.
Create the app as part of the Learneroo Ruby Coding Contest. Coming soon: A Tutorial on creating the complete app! Created with Rails App Composer, Devise and Bootstrap. Ready for deployment on Heroku.

License
-------
Copyright 2015 Ariel Krakowski. Free to use in your own applications...